package retele.clientserver;

import java.net.*;
import java.io.*;
import java.util.Date;


public class Server {

    public void startServer() throws IOException {
        ServerSocket ss = new ServerSocket(888);
        System.out.println("Serverul asteapta conexiuni...");
        Socket client = ss.accept();
        System.out.println("Clientul s-a conectat!");

        BufferedReader xin =
                new BufferedReader(new InputStreamReader(client.getInputStream()));

        PrintWriter xout =
                new PrintWriter(new OutputStreamWriter(client.getOutputStream()),true);

        // --------------------

        String msg = xin.readLine();
        while(!msg.equals("bye")){
            String response = "";
            if(msg.equals("request_time")){
                Date d = new Date();
                response = "Time is "+d.toString();
            }
            else if(msg.equals("echo")){
                response = "Echo from server!";
            }
            else{
                response = "Command unknown!";

            }
            xout.println(response);
            msg = xin.readLine();
        }
        System.out.println("Conexiune terminata.");
        client.close();
    }

    public static void main(String[] args) throws IOException {
        Server s1 = new Server();
        s1.startServer();
    }
}
