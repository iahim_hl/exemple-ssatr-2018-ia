package retele.clientserver;


import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;

public class ServerMultithread {
    public void startServer() throws IOException {
        ServerSocket ss = new ServerSocket(888);
        while(true) {
            System.out.println("Serverul asteapta conexiuni...");
            Socket client = ss.accept();
            System.out.println("Clientul s-a conectat!");
            ClientHandler handler = new ClientHandler(client);
            handler.start();
        }//.while
    }

    public static void main(String[] args) throws IOException {
        new ServerMultithread().startServer();
    }
}

/////////////////////////////////////////////////////

class ClientHandler extends Thread{

    Socket s = null;

    public ClientHandler(Socket s) {
        this.s = s;
    }

    public void run(){
    try {
        BufferedReader xin =
                new BufferedReader(new InputStreamReader(s.getInputStream()));

        PrintWriter xout =
                new PrintWriter(new OutputStreamWriter(s.getOutputStream()), true);

        // --------------------

        String msg = xin.readLine();
        while (!msg.equals("bye")) {
            String response = "";
            if (msg.equals("request_time")) {
                Date d = new Date();
                response = "Time is " + d.toString();
            } else if (msg.equals("echo")) {
                response = "Echo from server!";
            } else {
                response = "Command unknown!";

            }
            xout.println(response);
            msg = xin.readLine();
        }
        System.out.println("Conexiune terminata.");
        s.close();
    }catch(Exception e){
        e.printStackTrace();
    }
    }
}
