package retele.clientserver;

import java.net.*;
import java.io.*;
import java.util.Scanner;

public class Client {

    public void conectareClient() throws IOException {
        System.out.println("Conectare la server...");
        Socket s = new Socket("127.0.0.1",888);
        System.out.println("Conectare realizata!");

        BufferedReader xin =
                new BufferedReader(new InputStreamReader(s.getInputStream()));

        PrintWriter xout =
                new PrintWriter(new OutputStreamWriter(s.getOutputStream()),true);

        // --------------------

        Scanner scan = new Scanner(System.in);
        String line = scan.nextLine();
        while(!line.equals("exit")){
            xout.println(line);
            String response = xin.readLine();
            System.out.println("Server:"+response);
            line = scan.nextLine();
        }

    }

    public static void main(String[] args) throws IOException {
        Client c = new Client();
        c.conectareClient();



    }

}
