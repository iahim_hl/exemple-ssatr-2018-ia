import java.net.*;
import java.io.*;
import java.util.Scanner;

public class Client {

    static String readMessage(){
        Scanner sc = new Scanner(System.in);
        return sc.nextLine();
    }


    public static void main(String[] args) throws IOException {
        System.out.println("Clientul se conecteaza...");
        Socket s = new Socket("localhost",1888);

        System.out.println("Clientul s-a conectat la server!");

        PrintWriter k =
                new PrintWriter(new OutputStreamWriter(s.getOutputStream()),true);
        BufferedReader y =
                new BufferedReader(new InputStreamReader(s.getInputStream()));

        String m = readMessage();
        while(!m.equalsIgnoreCase("exit")){
            k.println(m);
            String raspuns = y.readLine();
            System.out.println(raspuns);
            m = readMessage();
        }

    }
}
