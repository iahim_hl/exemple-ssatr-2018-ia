import java.net.*;
import java.io.*;
import java.util.Date;
// https://pastebin.com/Asmf4A2c

// https://pastebin.com/v7rP6bhF
public class Server {

    public static void main(String[] args) throws IOException {
        ServerSocket ss = new ServerSocket(1888);
        while(true) {
            System.out.println("Astept conexiune de la client...");
            Socket s = ss.accept();

            PrintWriter out =
                    new PrintWriter(new OutputStreamWriter(s.getOutputStream()), true);
            BufferedReader in =
                    new BufferedReader(new InputStreamReader(s.getInputStream()));
            while(true) {
                System.out.println("Astept mesaje de la client...");
                String line = in.readLine(); //asteapta mesaje de la client
                System.out.println("Am primti mesaj de la client.");
                if(line.equals("time")) {
                    Date d = new Date();
                    out.println(d.toString());
                }else {
                    line = line.toUpperCase();
                    out.println("Nu inteleg comanda!"); //trimit raspuns la client
                }
            }

        }
    }
}
