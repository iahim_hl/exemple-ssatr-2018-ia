package oop.firexeccutie;

public class Main {

    public static void main(String[] args) throws InterruptedException {

        ConveyorBelt b = new ConveyorBelt();
        ConveyorBelt c = new ConveyorBelt();
        Thread t1 = new Thread(b);
        //t1.start();

	   Robot r1 = new Robot("R1", 10,10, b);
       r1.start();

       Robot r2 = new Robot("R2", 70,90, c);
       r2.start();

       RobotsBoard board = new RobotsBoard();

        board.addRobot(r1);
        board.addRobot(r2);


        System.out.println("Kill robots!");
        Thread.sleep(100000);
        r1.stopRobot();
        r2.stopRobot();



    }
}
