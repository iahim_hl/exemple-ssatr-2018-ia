package oop.firexeccutie;


import java.util.Random;

public class Robot extends Thread{
    private final String name;
    private int x, oldX;
    private int y, oldY;

    private boolean active = true;
    private ConveyorBelt conveyorBelt;

    public Robot(String name, int x, int y, ConveyorBelt conveyorBelt) {
        this.name = name;
        this.x = x;
        this.y = y;
        this.setName("Robot "+name);
        this.conveyorBelt = conveyorBelt;
    }

    @Override
    public void run(){
        while(active){
            move();
            //conveyorBelt.applyOperation();
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    Random r = new Random();
    public void move(){
        oldX = x;
        oldY = y;

        x= x + 2 - r.nextInt(3);
        y = y + + 2 - r.nextInt(3);
        System.out.println(this);
    }

    public void stopRobot(){
        active = false;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getOldX() {
        return oldX;
    }

    public void setOldX(int oldX) {
        this.oldX = oldX;
    }

    public int getOldY() {
        return oldY;
    }

    public void setOldY(int oldY) {
        this.oldY = oldY;
    }

    @Override
    public String toString() {
        return "Robot{" +
                "name='" + name + '\'' +
                ", x=" + x +
                ", y=" + y +
                '}';
    }
}
