package oop.firexeccutie;


public class ConveyorBelt implements Runnable {

    int k = 1;

    public ConveyorBelt() {

    }

    public synchronized void  applyOperation(){
        k = k - 1;
        System.out.println("Start doing operation! "+k+" "+Thread.currentThread().getName());

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Stop doing operation! "+k+" "+Thread.currentThread().getName());
        k = k + 1;
    }

    public void run(){
        while(true){
            System.out.println("Conveyor moving...");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
