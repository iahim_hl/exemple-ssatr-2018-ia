package oop.firexeccutie;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class RobotsBoard extends JFrame implements Runnable {

    ArrayList<Robot> list = new ArrayList<>();

    public RobotsBoard() throws HeadlessException {
        setSize(800,800);
        setTitle("Robots moving...");
        setVisible(true);
        Thread t = new Thread(this);
        t.start();
    }

    public void addRobot(Robot r){
        list.add(r);
    }

    public void paint(Graphics g){
        //g.drawOval(90,90,50,50);
        for(Robot r:list){
            g.setColor(Color.white);
            g.fillOval(r.getOldX(),r.getOldY(),50,50);
            g.setColor(Color.black);
            g.fillOval(r.getX(),r.getY(),50,50);
        }
    }

    @Override
    public void run() {
        while(true){
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            repaint();
        }
    }
}
