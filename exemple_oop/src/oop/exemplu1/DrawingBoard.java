package oop.exemplu1;

import javax.swing.*;
import java.awt.*;

/**
 * @author mihai.hulea
 */
public class DrawingBoard extends JFrame implements Runnable{

    int x, xOld;
    int y, yOld;

    public DrawingBoard() throws HeadlessException {
        setTitle("Robots");
        setSize(400,400);
        x = 80;
        y = 80;
        setVisible(true);
        Thread t= new Thread(this);
        t.start();
    }

    public void paint(Graphics g){
        g.setColor(Color.white);
        g.drawRect(xOld,yOld,50,50);
        g.setColor(Color.black);
        g.drawRect(x,y,50,50);

    }

    public static void main(String[] args) {
        new DrawingBoard();
    }

    @Override
    public void run() {
        while(true){
            xOld = x;
            yOld = y;
            x+=3;
            y+=3;
            this.repaint();
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
