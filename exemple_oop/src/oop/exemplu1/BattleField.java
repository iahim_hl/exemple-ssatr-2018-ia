package oop.exemplu1;

import java.util.ArrayList;

public class BattleField {
    private ArrayList<Robot> robots = new ArrayList<>();

    void addRobot(Robot r){
        robots.add(r);
    }

    void showRobots(){
        for(Robot x:robots)
            x.showPosition();
    }

    void fireRobot(String name){
        Robot active = null;
        for(Robot x: robots)
            if(x.getName().equals(name))
                active = x;

        if(active!=null){
            Bomb b = active.fire(); // (0,1), (1,0), (-1,0), (0,-1)
            b.setX(active.getX());
            b.setY(active.getY());
            for(int i=0;i<10;i++){
                b.increment();
                for(Robot y:robots){
                    if(y.isHit(b))
                        robots.remove(y);//eroare la stergere!!!
                }
            }
        }

    }
}
