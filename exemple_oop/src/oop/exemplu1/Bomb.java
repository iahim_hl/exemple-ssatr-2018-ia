package oop.exemplu1;

/**
 * @author mihai.hulea
 */
public class Bomb {
    int startX;
    int startY;
    int x;
    int y;

    public Bomb(int x, int y) {
        this.x = x;
        this.y = y;
        startX = x;
        startY = y;
    }

    void increment(){
        x += startX;
        y += startY;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
