package oop.exemplu1;

import java.util.Random;

/**
 * https://pastebin.com/3VZUnsxY
 * https://pastebin.com/0YEAFMmd
 * 1 - la initializare capacitatea bateriei trebuie sa fie 10
 * 2 - daca capacitatea <=0 robotoul nu se poate deplasa
 * 3 - robotul trebuie sa definieasca o metoda de incarcare a bateriei cu o valaore intre 1 si 10
 *
 */

public class Robot {
    private String name;
    private int x;
    private int y;
    private int charge;

    public Robot(String name, int x, int y) {
        this.name = name;
        this.x = x;
        this.y = y;
    }

    String getName(){
        return name;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    boolean isHit(Bomb b){
        return b.getX()==this.x&&b.getY()==this.y;
    }

    int getChrageLevel(){
        return charge;
    }

    void move(int dirX, int dirY){
        x = x + dirX;
        y = y + dirY;
        charge--;
        showPosition();
    }

    Bomb fire(){
        Random r = new Random();
        return new Bomb(0,1);
        //return r.nextInt(4);
    }

    void showPosition(){
        System.out.println("Robot x="+x+" y="+y+" name="+name);
    }

    public static void main(String[] args) {
        Robot r1 = new Robot("AB1",10,20);
        r1.move(1,0); //-1 0 1
        r1.move(-1,1);
        r1.move(111,-90);

        BattleField b1 = new BattleField();
        b1.addRobot(new Robot("EE2",10,5));
        b1.addRobot(r1);
        b1.addRobot(new Robot("XY4", 7,9));
        b1.showRobots();

    }
}
