/**
 * https://pastebin.com/97rDzjU9
 */
public class Main2 {
    public static void main(String[] args) {

        RobotBoard board = new RobotBoard();

        Robot r1 = new Robot("R1", 150,100);
        Robot r2 = new Robot("R2", 200,200);
        board.addRobot(r1);
        board.addRobot(r2);
        board.drawAll();
        Charger w = new Charger();
        Charger w2 = new Charger();
        r1.setCharger(w);
        r2.setCharger(w2);
        r1.start();
        r2.start();



    }
}
