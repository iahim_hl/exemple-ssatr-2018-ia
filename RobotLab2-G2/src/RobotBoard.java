import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Iterator;

public class RobotBoard extends JFrame {
    int MAX_X = 900;
    int MAX_Y = 900;
    ArrayList<Robot> list = new ArrayList<>();
    int[][] pos = new int[900][900];

    RobotBoard(){
        setTitle("Robots Board");
        setSize(500,500);
        setVisible(true);
    }

    void drawAll(){
        repaint();
    }

    public void paint(Graphics g){
        System.out.println("Drawing all robots...");
        for(Robot r: list)
            r.drawRobot(g);
    }

    void addRobot(Robot r){
        if(r.getX()<=MAX_X&&r.getY()<=MAX_Y) {
            r.setG(this.getGraphics());
            list.add(r);
            pos[r.getX()][r.getY()] = 1;
        }
        else
            System.out.println("Wrong coordinates!");
    }

    void removeRobot(String name){
        Iterator<Robot> i = list.iterator();
        while(i.hasNext()){
            Robot r = i.next();
            if(r.getName().equals(name)) {
                pos[r.getX()][r.getY()] = 0;
                i.remove();
            }
        }
    }

    void fire(String name, int direction){
        //........
    }

    void lockupAndFire(String name){
        //........
    }

//    void addRobot(String n, int x, int y){
//        list.add(new Robot(n, x, y));
//    }

    void displayAll(){
        for (int i = 0; i < list.size(); i++) {
            list.get(i).display();
        }
    }

    void findAndDisplay(String name){
        System.out.println("Find and display robot by name...");
        for(Robot r: list){
            if(r.getName().equals(name))
                r.display();
        }
    }

    void moveRobot(String name, int p, int q){
        System.out.println("Move robot...");
        for(Robot r: list){
            if(r.getName().equals(name)) {
                pos[r.getX()][r.getY()] = 0;
                r.move(p, q);
                pos[r.getX()][r.getY()] = 1;
            }
        }
    }

    void displayRobotMatrix(){
        for(int i =0;i<MAX_X;i++){
            for(int j = 0;j<MAX_Y;j++)
                System.out.print(" "+pos[i][j]);
            System.out.println();
        }
        System.out.println("----------------------");
    }
}
