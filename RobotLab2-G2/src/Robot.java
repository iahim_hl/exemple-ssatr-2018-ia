import java.awt.*;
import java.util.Random;

public class Robot extends Thread {
    private String name;
    private int x;
    private int y;
    private Charger c;
    private Graphics g;

    public Robot(String name, int x, int y) {
        this.name = name;
        this.x = x;
        this.y = y;
    }

    public void setCharger(Charger c){
        this.c = c;
    }

    public void setG(Graphics g) {
        this.g = g;
    }

    public void run(){
        Random r = new Random();
        int k = 0;
        while(k<10){
            k++;
            try{Thread.sleep(1000);}catch(Exception e){}
            int a = -5+r.nextInt(10);
            int b = -5+r.nextInt(10);
            move(a,b);
            drawRobot();
            display();
        }
        c.charge();
    }

    public void drawRobot(){
        System.out.println("Draw "+name);
        g.drawOval(50+x,50+y, 80,80);
        g.drawString(this.name, 90+x,90+y);
    }

    public void drawRobot(Graphics g){
        System.out.println("Draw "+name);
        g.drawOval(50+x,50+y, 80,80);
        g.drawString(this.name, 90+x,90+y);
    }

    void move(int k, int j){
        this.x = x + k;
        this.y = y + j;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    void display(){
        System.out.println("I'm robot "+name+" on coordinates="+x+","+y);
    }
}
