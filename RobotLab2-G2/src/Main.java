public class Main {
//    https://pastebin.com/qjZdBt2A
    public static void main(String[] args) {
        Robot r1 = new Robot("AB1", 18, 9);
        RobotBoard b = new RobotBoard();

        b.addRobot(r1);
        b.displayRobotMatrix();
        Robot r2 = new Robot("AAA", 9, 9);
        Robot r3 = new Robot("ZZZ", 10, 7);

        b.moveRobot("AB1", 1,1);
        b.displayRobotMatrix();
        b.removeRobot("AB1");
        b.displayRobotMatrix();

//        Robot r1 = new Robot("AB1", 80, 90);
//        r1.display();
//        r1.move(0,10);
//        r1.display();
//        System.out.println(r1.getName()+" "+r1.getX());

//        System.out.println("Hello World!");
//        Robot r1 = new Robot("AB1", 80, 90);
//        Robot r2 = new Robot("XYZ", 100, 70);
//        Robot[] lista = new Robot[10];
//        lista[0] = new Robot("AABB3", 45, 20);
//        lista[1] = r1;
//        r1.display();
//        r2.display();
//        lista[0].display();
//        lista[1].display();
//        Robot r3 = r2;
//        r3.display();
//        Robot r4 = null;
//        r4.display();
//        lista[3].display();
    }
}
